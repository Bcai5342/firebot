import java.util.Arrays;

/**
 * info1103 - assignment 3 <Brenden Cai> <BCAI5342>
 */

public class Simulation {

	//
	// TODO
	//

	public enum windDirection {
		NONE, ALL, NORTH, EAST, SOUTH, WEST
	};

	private int seed;
	private int width;
	private int height;
	private windDirection currentWindDirection;
	private int pollution;
	private int day;
	private double damage = 0.0;
	private Tree[][] trees;

	public Simulation(int seed, int width, int height) {
		this.seed = seed;
		this.width = width; // LEFT-RIGHT
		this.height = height; // UP-DOWN
		this.pollution = 0;
		this.currentWindDirection = Simulation.windDirection.NONE;
		this.day = 1;
		this.trees = new Tree[height][width];
		this.generateTerrain(seed);
	}

	// GETTER/SETTER
	public int getSeed() {
		return this.seed;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public windDirection getCurrentWindDirection() {
		return this.currentWindDirection;
	}

	public int getPollution() {
		return this.pollution;
	}

	public int getDay() {
		return this.day;
	}

	public double getDamage() {
		processDamage();
		return this.damage;
	}

	public void setCurrentWindDirection(windDirection windDir) {
		this.currentWindDirection = windDir;
		System.out.println("Set wind to " + windDir.name().toLowerCase()+ "\n");
	}

	private void generateTerrain(int seed) {

		// ###################################
		// ### DO NOT MODIFY THIS FUNCTION ###
		// ###################################

		Perlin perlin = new Perlin(seed);
		double scale = 10.0 / Math.min(width, height);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				double height = perlin.noise(x * scale, y * scale, 0.5);
				height = Math.max(0, height * 1.7 - 0.7) * 10;
				trees[y][x] = new Tree((int) height);
			}
		}
	}
	
	// Start fire (All)
	public void startFire() throws Exception{
		startFire(0,0,height,width);
	}

	// Start fire (Single)
	public void startFire(int row, int column) throws Exception {
		startFire(row,column,row,column);
	}

	// Start fire (region)
	public void startFire(int row, int column, int row2, int column2) throws Exception {
		boolean fireStarted = false;
		
		if ((row >= 0) && (column >= 0) && (row2 >= row) && (column2 >= column) && (row2 < height) && (column2 < width)) {
			for (int rowPosition = row; rowPosition <= row2; rowPosition++) {
				for (int columnPosition = column; columnPosition <= column2; columnPosition++) {
					if (this.trees[rowPosition][columnPosition].setAlight()) {
						fireStarted = true;
					}
				}
			}
		} else {
			throw new Exception("Out of Bounds");
		}
		if (fireStarted) {
			System.out.println("Started a fire\n");
		} else {
			System.out.println("No fires were started\n");
		}
	}

	public void extinguishFire() { // EXTINGUISH ALL
		boolean fireExtinguished = false;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (this.trees[i][j].extinguish()) {
					fireExtinguished = true;
				}
			}
		}
		if (fireExtinguished) {
			System.out.println("Extinguished fires"); // TODO
		} else {
			System.out.println("No fires to extinguish"); // TODO
		}
	}

	public void extinguishFirea(int row, int column) throws Exception {
		this.extinguishFire(row, column, row, column);
	}

	public void extinguishFire(int row, int column, int row2, int column2) throws Exception {
		boolean fireExtinguished = false;
		
		if ((row >= 0) && (column >= 0) && (row2 >= row) && (column2 >= column) && (row2 < height) && (column2 < width)) {
			for (int rowPosition = row; rowPosition <= row2; rowPosition++) {
				for (int columnPosition = column; columnPosition <= column2; columnPosition++) {
					if (this.trees[rowPosition][columnPosition].extinguish()) {
						fireExtinguished = true;
					}
				}
			}
		} else {
			throw new Exception("Out of Bounds");
		}
		if (fireExtinguished) {
			System.out.println("Extinguished fires\n"); // FIXME
		} else {
			System.out.println("No fires to extinguish\n");
		}
	}

	public void processPollution() {
		for (Tree[] subTrees : trees) {
			for (Tree tree : subTrees) {
				pollution = pollution + tree.calculatePollutionOffset();
			}
		}
		if(this.pollution < 0) {
			this.pollution = 0;
		}
	}

	public void printStatus() {
		System.out.println("Day: " + this.getDay());
		System.out.println("Wind: " + this.getCurrentWindDirection().toString().toLowerCase());
		System.out.println();
	}

	public void tick(int iterations) {
		for (int i = 0; i < iterations; i++) {
			tick();
		}
		this.printStatus();
	}

	public void tick() {
		Tree[][] newTrees = new Tree[height][width];
		for (int i = 0; i < trees.length; i++) {
			for (int j = 0; j < trees[i].length; j++) {
				newTrees[i][j] = trees[i][j].copyOf();
			}
		}

		switch (this.currentWindDirection) {
		case NONE: // IDEAL case where no wind, no spread.
			tickFires(newTrees);
			break;
		case NORTH: // (-1,0) is North of current tree.
			tickFires(newTrees);
			spreadFires(newTrees, -1, 0);
			break;
		case SOUTH:
			tickFires(newTrees);
			spreadFires(newTrees, 1, 0);
			break;
		case EAST:
			tickFires(newTrees);
			spreadFires(newTrees, 0, 1);
			break;
		case WEST:
			tickFires(newTrees);
			spreadFires(newTrees, 0, -1);
			break;
		case ALL:
			tickFires(newTrees);
			spreadFires(newTrees, -1, 0);
			spreadFires(newTrees, 1, 0);
			spreadFires(newTrees, 0, 1);
			spreadFires(newTrees, 0, -1);
			break;
		default:
			break;
		}
		trees = newTrees;
		this.day++;
		processPollution();
	}

	private void spreadFires(Tree[][] newTrees, int rowOffset, int columnOffset) {
		for (int i = 0; i < newTrees.length; i++) {
			for (int j = 0; j < newTrees[i].length; j++) {
				spreadFireDirected(newTrees, i, j, rowOffset, columnOffset);
			}
		}
	}

	private void tickFires(Tree[][] newTrees) {
		for (int i = 0; i < newTrees.length; i++) {
			for (int j = 0; j < newTrees[i].length; j++) {
				tickFire(newTrees, i, j);
			}
		}
	}

	private void tickFire(Tree[][] newTrees, int row, int column) {
		Tree oldTree = trees[row][column];
		Tree currentTree = newTrees[row][column];

		if (oldTree.getFireIntensity() > 0 && !oldTree.getBurntStatus() && oldTree.getHeight() > 0) {
			if (oldTree.getFireIntensity() < 9) { // Increase Fire
				currentTree.setFireIntensity(oldTree.getFireIntensity() + 1);
			} else if (oldTree.getFireIntensity() == 9) { // MaxFire
				currentTree.setHeight(oldTree.getHeight() - 1);
				if (currentTree.getHeight() == 0) { // BURNT DOWN
					currentTree.setBurntDown(true);
					currentTree.setFireIntensity(0);
				}
			}
		}
	}

	private void spreadFireDirected(Tree[][] newTrees, int row, int column, int rowOffset , int coloumnOffset) {

		// Check if we even need to spread fire
		Tree oldTree = trees[row][column];
		if (oldTree.getFireIntensity() > 0) {

			// check if (that) direction is a valid location
			int newRow = row + rowOffset;
			int newColumn = column + coloumnOffset;

			if ((newRow < height && newRow >= 0) && (newColumn < width) && (newColumn >= 0)) { // Bounding
																				// Box
				Tree currentTree = newTrees[newRow][newColumn];
				// Not empty space, Not burnt down, Is not already burning.
				if (currentTree.getHeight() > 0 && currentTree.getFireIntensity() == 0) {
					currentTree.setAlight();
				}
			}
		}
	}

	public void processDamage() {
		double treeCount = 0;
		double burntDown = 0;
		for (Tree[] treesA : trees) {
			for (Tree treeB : treesA) {
				if (treeB.getBurntStatus() || treeB.getHeight() > 0) {
					treeCount++;
				}
				if (treeB.getBurntStatus()) {
					burntDown++;
				}
			}
		}

		this.damage = (burntDown / treeCount)*100.0;
	}

	public void graph(String type) {
		if (type.toLowerCase().equals("fire")) {
			printHorizontalBorder();
			for (int x = 0; x < height; x++) {
				System.out.print("|");
				for (int y = 0; y < width; y++) {
					Tree tree = this.trees[x][y];
					if (tree.getHeight() > 0 && tree.getFireIntensity() > 0) {
						System.out.print(tree.getFireIntensity());
					} else if (tree.getHeight() == 0 && tree.getBurntStatus()) {
						System.out.print("x");
					} else if (tree.getHeight() > 0 && tree.getFireIntensity() == 0) {
						System.out.print(".");
					} else {
						System.out.print(" ");
					}
					if (y != width - 1) {
						System.out.print(" ");
					}
				}
				System.out.print("|\n");
			}
			printHorizontalBorder();
		}
		if (type.toLowerCase().equals("height")) {
			printHorizontalBorder();
			for (int x = 0; x < height; x++) {
				System.out.print("|");
				for (int y = 0; y < width; y++) {
					if (trees[x][y].getHeight() > 0) {
						System.out.print(trees[x][y].getHeight());
					} else if (trees[x][y].getBurntStatus()) {
						System.out.print("x");
					} else {
						System.out.print(" ");
					}
					if (y != width - 1) {
						System.out.print(" ");
					}
				}
				System.out.print("|\n");
			}
			printHorizontalBorder();
		}
		System.out.println();
	}

	private void printHorizontalBorder() {
		System.out.print("+");
		for (int i = 0; i < width * 2 - 1; i++) {
			System.out.print("-");
		}
		System.out.print("+");
		System.out.println();
	}
}
