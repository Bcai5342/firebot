
/**
 * info1103 - assignment 3 <Brenden Cai> <BCAI5342>
 */

import java.util.Scanner;

public class Firebot {

	//
	// TODO
	//

	private static Scanner scan;
	private static Simulation sim;

	public static void main(String[] args) {
		if (!verifyInputArgs(args)) {
			System.out.println("Usage: java Firebot <seed> <width> <height>");
			System.exit(1);
		}

		// Casting from String to integer variables
		int seed = Integer.parseInt(args[0]);
		int width = Integer.parseInt(args[1]);
		int height = Integer.parseInt(args[2]);

		sim = new Simulation(seed, width, height);

		sim.printStatus();
		System.out.print("> ");

		scan = new Scanner(System.in);
		while (scan.hasNextLine()) {
			String input = scan.nextLine();
			input = input.toLowerCase();
			input = input.replace("> ", "");
			String[] inputArgs;
			if (!input.isEmpty()) {
				try {
					inputArgs = input.split(" ");

					switch (inputArgs[0]) { // TODO
					case "bye":
						System.out.println("bye");
						System.exit(0);
						break;
					case "help":
						System.out.print("BYE\n" + "HELP\n\n" + "DATA\n" + "STATUS\n\n");
						System.out.print("NEXT <days>\n" + "SHOW <attribute>\n\n");
						System.out.print("FIRE <region>\n" + "WIND <direction>\n" + "EXTINGUISH <region>");
						System.out.println();
						System.out.println();
						break;
					case "data":
						System.out.printf("Damage: %.2f%%\n", sim.getDamage());
						System.out.printf("Pollution: %d\n", sim.getPollution());
						System.out.println();
						break;
					case "status":
						sim.printStatus();
						break;
					case "next":
						if (inputArgs.length == 2) {
							sim.tick(Integer.parseInt(inputArgs[1]));
						} else {
							sim.tick(1);
						}
						break;
					case "show":
						if (inputArgs[1].equals("fire")) {
							sim.graph("fire");
						} else if (inputArgs[1].equals("height")) {
							sim.graph("height");
						} else {
							throw new Exception("Invalid command");
						}
						break;
					case "fire": //trees[row-y][column-x] but [
						if (inputArgs.length == 1) {
							sim.startFire();
						} else if (inputArgs.length == 3) { //COLUMN -Y THEN ROW-X
							sim.startFire(Integer.parseInt(inputArgs[2]), Integer.parseInt(inputArgs[1]));
						} else if (inputArgs.length == 5) {
							sim.startFire(Integer.parseInt(inputArgs[2]), Integer.parseInt(inputArgs[1]),
									Integer.parseInt(inputArgs[4]), Integer.parseInt(inputArgs[3]));
						}
						break;
					case "wind":
						if (inputArgs.length == 2) {
							if (inputArgs[1].toLowerCase().equals("north")) {
								sim.setCurrentWindDirection(Simulation.windDirection.NORTH); // FIXME
							} else if (inputArgs[1].toLowerCase().equals("south")) {
								sim.setCurrentWindDirection(Simulation.windDirection.SOUTH); // FIXME
							} else if (inputArgs[1].toLowerCase().equals("east")) {
								sim.setCurrentWindDirection(Simulation.windDirection.EAST); // FIXME
							} else if (inputArgs[1].toLowerCase().equals("west")) {
								sim.setCurrentWindDirection(Simulation.windDirection.WEST); // FIXME
							} else if (inputArgs[1].toLowerCase().equals("all")) {
								sim.setCurrentWindDirection(Simulation.windDirection.ALL); // FIXME
							} else if (inputArgs[1].toLowerCase().equals("none")) {
								sim.setCurrentWindDirection(Simulation.windDirection.NONE); // FIXME
							} else {
								throw new Exception(); //TODO nothing to write in exception
							}
						}
						break;
					case "extinguish":
						if (inputArgs.length == 1) {
							sim.extinguishFire();
						} else if(inputArgs.length == 3) {
							sim.extinguishFirea(Integer.parseInt(inputArgs[2]), Integer.parseInt(inputArgs[1]));
						} else if(inputArgs.length == 5) {
							sim.extinguishFire(Integer.parseInt(inputArgs[2]), Integer.parseInt(inputArgs[1]), Integer.parseInt(inputArgs[4]), Integer.parseInt(inputArgs[3]));
						} else {
							throw new Exception(); //TODO nothing to write in exception
						}
						break;
					default:
						throw new Exception("Invalid command");
					}
				} catch (Exception e) {
					//System.out.print(e.getCause() + ":");
					//System.out.println(e.getMessage() + "\n");
					System.out.println("Invalid command\n");

				}
			}
			System.out.print("> ");
		}
	}

	public static boolean verifyInputArgs(String[] args) {
		try {
			if (args.length != 3) {
				return false;
			}
			if (Integer.parseInt(args[0]) < 1) {
				return false;
			}
			if (Integer.parseInt(args[1]) < 1) {
				return false;
			}
			if (Integer.parseInt(args[2]) < 1) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean verifyIntParams(String[] args, int expected) throws Exception {
		if (args.length == expected) {
			try {
				for (int i = 1; i < args.length; i++) {
					Integer.parseInt(args[i], 10);
				}
				return true;
			} catch (Exception e) {
				throw new Exception("Invalid command");
			}
		}
		return false;
	}
}
